package com.veeva.vault.custom.actions;

import com.veeva.vault.sdk.api.action.DocumentActionContext;
import com.veeva.vault.sdk.api.core.UserDefinedService;
import com.veeva.vault.sdk.api.core.UserDefinedServiceInfo;

@UserDefinedServiceInfo
public interface SendtoAEMAuthorService extends UserDefinedService {

    void invokeSyncServlet(DocumentActionContext documentActionContext, String query);
}
