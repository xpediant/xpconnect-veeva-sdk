package com.veeva.vault.custom.actions;

import com.veeva.vault.sdk.api.action.DocumentActionContext;
import com.veeva.vault.sdk.api.core.*;
import com.veeva.vault.sdk.api.document.DocumentVersion;
import com.veeva.vault.sdk.api.http.HttpMethod;
import com.veeva.vault.sdk.api.http.HttpRequest;
import com.veeva.vault.sdk.api.http.HttpResponseBodyValueType;
import com.veeva.vault.sdk.api.http.HttpService;


@UserDefinedServiceInfo
public class SendtoAEMAuthorServiceImpl implements SendtoAEMAuthorService {

    @Override
    public void invokeSyncServlet(DocumentActionContext documentActionContext, String apiName) {
        LogService logService = ServiceLocator.locate(LogService.class);
        HttpService httpService = ServiceLocator.locate(HttpService.class);

        DocumentVersion document = documentActionContext.getDocumentVersions().get(0);
        String id  = document.getValue("id", ValueType.STRING);

        HttpRequest request = httpService.newHttpRequest(apiName);
        request.setMethod(HttpMethod.GET);
        request.setQuerystringParam("id",id);
        request.setHeader("XpConnectSessionId", "${Session.SessionId}");
        request.setResolveTokens(true);
        httpService.send(request, HttpResponseBodyValueType.STRING)
                .onSuccess(httpResponse -> {
                    logService.debug("Entry action- 'XpConnect® - Send to AEM Author' has been executed for Document with ID: "+id+". "+httpResponse);
                    logService.info("Send to AEM Author is queued to download.");
                }).onError(httpOperationError -> {
                    logService.debug("error: Received finishing Send to AEM Author"+ httpOperationError.getHttpResponse() +"Error"+httpOperationError.getMessage());
        }).execute();
    }

}
