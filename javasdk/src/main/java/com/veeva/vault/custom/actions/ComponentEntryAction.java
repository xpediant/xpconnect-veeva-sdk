package com.veeva.vault.custom.actions;

import com.veeva.vault.sdk.api.action.DocumentAction;
import com.veeva.vault.sdk.api.action.DocumentActionContext;
import com.veeva.vault.sdk.api.action.DocumentActionInfo;
import com.veeva.vault.sdk.api.action.Usage;
import com.veeva.vault.sdk.api.core.ServiceLocator;


@DocumentActionInfo(lifecycle = "component1__c", label="XpConnect®: Component Send to AEM Author", icon="sync__sys", usages={Usage.LIFECYCLE_ENTRY_ACTION})
public class ComponentEntryAction implements DocumentAction {

	@Override
	public boolean isExecutable(DocumentActionContext documentActionContext) {
		return true;
	}

	@Override
	public void execute(DocumentActionContext documentActionContext) {
		SendtoAEMAuthorService sendtoAEMAuthorService = ServiceLocator.locate(SendtoAEMAuthorService.class);
		sendtoAEMAuthorService.invokeSyncServlet(documentActionContext, "sync_component_asset");
	}


}